# `analytics_infra`

This is the infrastructure package for analytics app deployment

# Install 
1. You will need a unix-based system with `docker` and `docker-compose` installed.
2. Run the following commands to start the app:
    - ```docker-compose -f docker-compose.prod.yml up -d --build```
    - ```docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput```
    
# Further improvements
- move from `docker-compose` to `ansible`
- move from `sqlite3` to `postgres`