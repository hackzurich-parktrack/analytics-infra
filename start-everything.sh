#!/bin/bash

cd analytics-container/analytics-api
git pull origin master
cd ../../
git pull origin master

docker-compose -f docker-compose.prod.yml up -d --build
docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput